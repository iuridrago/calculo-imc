package classes;

import java.io.*;

public class LerEscreverCSV {
    public static void main(String[] args) {
        //String arquivoCSV = "C:\\Users\\Dragonous\\Documents\\calculo-imc\\dataset.csv";

        //Atribuindo um relative path
        String arquivoCSV = "dataset.CSV";
        BufferedReader conteudo = null;
        String linha = "";
        String separadorCampos = ";";
        int contador = 0;

        try{
            conteudo = new BufferedReader(new FileReader(arquivoCSV));
            while ((linha = conteudo.readLine()) != null)
            {
                contador++;
                //Pular os headers da tabela CSV
                if (contador == 1){
                    continue;
                }
                else {
                    //Corta a string no carácter informado, e joga no array de strings, 1 em cada indice
                    String[] array = linha.split(separadorCampos);
                  //Verificar e tratar as linhas que não vem com todas as informações
                    if (array.length < 4) {
                        System.out.println("Os dados estão incompletos na linha " + contador + ".");
                    } else {
                    /*
                    System.out.println("[Nome: "+array[0]
                            +", Sobrenome: "+array[1]
                            +", Peso: "+array[2]
                            +", Altura: "+array[3]+"]");

                     */
                        //System.out.println(array[0].toUpperCase()+" "+array[1].toUpperCase());
                        organizarArray(array);
                    }
                }

            }
            System.out.println("Arquivo txt gerado com sucesso!");

        }catch (FileNotFoundException e){
            System.out.println("O arquivo não foi encontrado: "+e.getMessage());
        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println("O indice do array está fora do limite: "+e.getMessage());
        }catch (IOException e){
            System.out.println("IO Erro: "+e.getMessage());
        }finally {
            if (conteudo != null){
                try{
                    conteudo.close();
                }catch (IOException e){
                    System.out.println("IO Erro: "+e.getMessage());
                }
            }
        }
    }

    public static void organizarArray(String[] vetor){
        Pessoas pessoa = new Pessoas(vetor[0],vetor[1],vetor[2],vetor[3]);
        String sobrenomeTrim = (pessoa.getSobrenome()).trim();
        String nomeSobrenomeTratado = ((pessoa.getNome()).replaceAll("\\s+","")+" "+(sobrenomeTrim).replaceAll("\\s{2,}"," ")).toUpperCase();
        String imcOK = pessoa.calcularIMC(pessoa.getPeso(), pessoa.getAltura());
        String tudoAqui = (nomeSobrenomeTratado+" "+imcOK);
        gerarArquivoTexto(tudoAqui);
    }

    public static void gerarArquivoTexto(String texto) {
        try {
            //FileWriter writer = new FileWriter("iuriRodriguesSantos.txt", true);
            BufferedWriter escritor = new BufferedWriter(new FileWriter("iuriRodriguesSantosSEGUNDO.txt", true));
            String[] textoFimDeLinha = texto.split("\n");

            for (String textoFOR: textoFimDeLinha)
            {
                //ESCREVE NO ARQUIVO
                escritor.write(textoFOR);
                //INSERE UMA NOVA LINHA
                escritor.newLine();
            }
            escritor.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
