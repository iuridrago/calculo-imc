package classes;

import java.text.DecimalFormat;

public class Pessoas {
    static DecimalFormat df = new DecimalFormat("###,##0.00");
    private String nome;
    private String sobrenome;
    private String peso;
    private String altura;

    public Pessoas(String nome, String sobrenome, String peso, String altura) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.peso = peso;
        this.altura = altura;
    }

    //GETTERS
    public String getNome() { return this.nome;    }
    public String getSobrenome() { return this.sobrenome; }
    public String getPeso() { return this.peso; }
    public String getAltura() { return this.altura; }

    //SETTERS
    public void setNome(String nome) { this.nome = nome; }
    public void setSobrenome(String sobrenome) { this.sobrenome = nome; }
    public void setPeso(String peso) { this.peso = peso; }
    public void setAltura(String altura) { this.altura = altura; }

    public static String calcularIMC(String peso, String altura) {
        String peso2 = peso.replace(",", ".");
        String altura2 = altura.replace(",", ".");
        //System.out.println("Peso: "+peso2+" | Altura: "+altura2);

        double pesoDouble = Double.parseDouble(peso2);
        double alturaDouble = Double.parseDouble(altura2);

        double alturaQuadrado = Math.pow(alturaDouble, 2);
        double imc = pesoDouble / alturaQuadrado;
        //System.out.println(df.format(imc));
        return df.format(imc);
    }
}
