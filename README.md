> **Exercício proposto**

Desenvolver e apresentar o código para calcular o IMC (índice de massa corporal - peso/altura²) de um grupo de pessoas cujos dados estão no arquivo dataset.CSV. 
Utilize este link para baixar o arquivo com os dados para o cálculo: "dataset.CSV ".   (não serão aceitos outros dados).  

É desejável que você grave um arquivo com os resultados obtidos e os apresente na reunião com o seu código.

> **Especificação do resultado a ser entregue**

Desenvolver código que calcule o IMC - índice de massa corporal de cada pessoa: IMC = massa/altura²
Formatar e imprimir o nome completo de cada pessoa com seu IMC ao lado direito
O nome completo deve ser escrito com todas as letras maiúsculas
O IMC deve estar separado do nome completo por um espaço em branco
O IMC deve estar com duas casas decimais, usando a vírgula como separador

**Exemplo de saída esperada:** JOÃO DA SILVA BEZERRA 24,72

> **Passo extra:**

Salve os dados gerados no arquivo:  [meuNomeCompleto].txt
